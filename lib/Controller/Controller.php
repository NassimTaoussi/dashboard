<?php

namespace Nassim\Lib\Controller;
require '../vendor/autoload.php';
use phpQuery;

abstract class Controller {

    protected $document;

    public function loadHtml($html) {
        $this->document = phpQuery::newDocument($html);
    }

    public function getDocument() {
        return $this->document;
    }

}

?>