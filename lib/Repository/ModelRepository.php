<?php

namespace Nassim\Lib\Repository;

use Nassim\Lib\Model\Database;

class ModelRepository {
    
    protected $pdo;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
    }
}

?>