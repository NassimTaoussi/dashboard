<?php

namespace Nassim\Lib\Router;

class Router {

    private $routes = [];

    public function addRoute($method, $path, $controllerCall) {
        $this->routes[] = [
            'method' => $method,
            'path' => $path,
            'controllerCall' => $controllerCall
        ];
    }

    public function dispatch() {
        $method = $_SERVER['REQUEST_METHOD'];
        $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);

        foreach ($this->routes as $route) {
            if ($method === $route['method'] && preg_match($this->convertToRegex($route['path']), $uri, $matches)) {
                array_shift($matches); // Remove full match
                list($controller, $action) = explode('@', $route['controllerCall']);
                $controller = "Nassim\\Src\\Controller\\$controller";
                $controllerInstance = new $controller();
                call_user_func_array([$controllerInstance, $action], $matches);
                return;
            }
        }

        // If no route matched
        header("HTTP/1.0 404 Not Found");
        echo '404 Not Found';
    }

    private function convertToRegex($path) {
        return '/^' . str_replace(['/', '{id}'], ['\/', '(\d+)'], $path) . '$/';
    }
}

?>