<?php

use Nassim\Lib\Model\Database;
use Nassim\Src\Model\Article;

class DataArticle {
    
    protected $pdo;
    private $faker;

    public function __construct()
    {
        $this->pdo = Database::getPdo();
        $this->faker = Faker\Factory::create('fr_FR');
    }

    public function launchFakerData() {
        $this->pdo->exec('TRUNCATE TABLE article');

        for ($i = 0; $i < 50; $i++) {
            $article = new Article($i, $this->faker->text(100), $this->faker->dateTime(), $this->faker->paragraph());

            $sql = "INSERT INTO article (title, content, created_at) 
                    VALUES (:title, :content, :created_at)";
            $query = $this->pdo->prepare($sql);
            $query->execute(
                array(
                    ':title' => $article->getTitle(),
                    ':content'=> $article->getContent(),
                    ':created_at'=> $article->getCreatedAt()->format('Y-m-d H:i:s'),
                )
            );
        }
    }
}

?>