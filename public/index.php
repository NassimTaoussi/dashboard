<?php

session_start();
session_regenerate_id();

require '../vendor/autoload.php';

use Nassim\Lib\Router\Router;
use Symfony\Component\Dotenv\Dotenv;
use Nassim\Lib\Controller\Controller;
use Nassim\Src\Controller\HomeController;


$dotenv = new Dotenv();
$dotenv->loadEnv(__DIR__ . '/../.env');

$router = new Router();

$router->addRoute('GET', '/', 'HomeController@index');

$router->addRoute('GET', '/dashboard', 'HomeController@dashboard');

$router->addRoute('GET', '/data/{id}', 'HomeController@getData');
$router->addRoute('GET', '/filter', 'HomeController@filterDataByDate');
$router->addRoute('POST', '/delete/{id}', 'HomeController@deleteOneData');


$router->dispatch();


?>