//POPUP

$(document).ready(function(){
    $(document).on('click', '.item', function() {
        var id = $(this).data('id');
        $.ajax({
            url: '/data/' + id,
            type: 'GET',
            success: function(response) {
                var data = JSON.parse(response);
                $('#popup-content').html(
                    "<ul>" +
                        "<li><span class='entitled'>N°" + data.article.id + "</span></li></br>" +
                        "<li><span class='entitled'>Title : </span>" + data.article.title + "</li></br>" +
                        "<li><span class='entitled'>Content : </span>" + data.article.content + "</li></br>" +
                        "<li><span class='entitled'>Created at : </span>" + data.article.created_at + "</li>" +
                    "</ul>"
                );
                $('#popup').removeClass('hidden');
            },
            error: function() {
                alert('Erreur lors du chargement des détails.');
            },
        });
    });

    
    $('#close-popup').on('click', function() {
        $('#popup').addClass('hidden');
    });

    
    filterData();
});

function filterData() {
    var formData = $("#filterForm").serialize();
    $.ajax({
        url: '/filter',
        type: 'GET',
        data: formData,
        success: function(data) {
            var articles = data;
            var articlesList = $("#articles-list");
            articlesList.empty();

            articles.forEach(function(article) {
                articlesList.append(
                    '<tr>' +
                        '<td>' + article.id + '</td>' + 
                        '<td class="item" data-id="' + article.id + '">' + article.title + '</td>' +
                        '<td>' + article.created_at + '</td>' + 
                        '<td><button class="deleteButton" data-id="' + article.id + '">Delete</button></td>' +
                    '</tr>'
                );
            });
            deletePopUp()
        },
        error: function(xhr, status, error) {
            console.error('Erreur lors de la récupération des données : ' + error);
        }
    });
}

function deletePopUp() {
    $(document).on('click', '.deleteButton', function() {
        var id = $(this).data('id');
        console.log(id);
        $('#popup-content').html(
                "<p>Do you want to delete the article n°" + id + " ?</p><br><br>" +
                "<button  onclick='confirmDelete(" + id + ")'>Yes</button>" 
        );
        
        $('#popup').removeClass('hidden');

        $('#close-popup').on('click', function() {
            $('#popup').addClass('hidden');
        });
    })
}

function confirmDelete(id) {
        $.ajax({
            url: '/delete/' + id,
            type: 'POST',
            success: function(response) {
                console.log('Article deleted.');
                $('#popup').addClass('hidden');
                filterData();
            },
            error: function() {
                console.log('Error when deleting.');
            }
        })
}