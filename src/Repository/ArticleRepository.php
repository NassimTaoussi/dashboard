<?php

namespace Nassim\Src\Repository;

use Nassim\Lib\Repository\ModelRepository;
use Nassim\Src\Model\Article;

class ArticleRepository extends ModelRepository {

    public function findAll(): array
    {
        $sql = "SELECT * FROM article ORDER BY id";
        $query = $this->pdo->prepare($sql);
        $query->execute();

        $articles = $query->fetchAll();
        array_map(function($article) {
            return new Article($article['id'], $article['title'], new \DateTime($article['created_at']), $article['content']);
        }, $articles);
        
        return $articles;
    }

    public function findOneById(int $id)
    {
        $sql = "SELECT article.id, title, content, created_at FROM article WHERE article.id = :id";
        $query = $this->pdo->prepare($sql);
        $query->bindParam(':id', $id, \PDO::PARAM_INT);
        $query->execute();
        $article = $query->fetch();
        return $article;
    }

    public function findAllByDateFilter($filter): array
    {

        if ($filter == 'recent') {
            $sql = "SELECT * FROM article ORDER BY created_at DESC";
        } elseif ($filter == 'ancient') {
            $sql = "SELECT * FROM article ORDER BY created_at ASC";
        } else {
            $sql = "SELECT * FROM article ORDER BY id";
        }


        $query = $this->pdo->prepare($sql);
        $query->execute();

        $articles = $query->fetchAll();
        array_map(function($article) {
            return new Article($article['id'], $article['title'], new \DateTime($article['created_at']), $article['content']);
        }, $articles);

        
        return $articles;
    }

    public function deleteById(int $id) {
        $query = $this->pdo->prepare("DELETE FROM article WHERE id = :id");
        return $query->execute(['id' => $id]);
    }

}

?>