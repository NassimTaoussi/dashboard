<?php

namespace Nassim\Src\Controller;

use Nassim\Lib\Controller\Controller as Controller;
use Nassim\Src\Repository\ArticleRepository;
use Illuminate\Http\Request;

class HomeController extends Controller {

    public function index() {
        // Load the HTML template 
        $template = file_get_contents('../public/templates/index.html');
        $this->loadHtml($template);

        // DOM manipulation with PHPQuery
        $this->document->find('#content')->append('<p>Welcome</p>');

        // Display of html edit
        echo $this->document->html();
    }

    public function dashboard() {
        // Get data from database
        $articleRepository = new ArticleRepository();
        $totalArticles = $articleRepository->findAll();
        $list = file_get_contents('../public/templates/list.html');
        $this->loadHtml($list);

        
        $articlesList = $this->document->find('#articles-list');

        foreach ($totalArticles as $article) {
            $articlesList->append(
                '<tr>' . 
                '<th scope="row">' . htmlspecialchars($article['id']) .
                '</th>' . '<td class="item" data-id="' . $article['id'] .'">' . htmlspecialchars($article['title']) . 
                '</td>' . '<td>' . htmlspecialchars($article['created_at']) . 
                '</td>' . '<td><button class="deleteButton" onclick="deletePopUp()" data-id="' . $article['id'] . '" >Delete</button></td> </tr>');
        }

        echo $this->document->html();
    }

    public function getData($id) {
        $articleRepository = new ArticleRepository();

        // Get one article by id
        $article = $articleRepository->findOneById($id);

        if (!$article) {
            // If the article was not found, return 404 code
            http_response_code(404);
            echo json_encode(['article' => 'Aucun détail trouvé pour cet élément.']);
            return;
        }
    
        // Si l'article est trouvé, retourner ses détails
        echo json_encode(['article' => $article]);
    }
    
    public function filterDataByDate() {
        $articleRepository = new ArticleRepository();

        // Get all article with date filter
        $articles = $articleRepository->findAllByDateFilter($_GET['filter'] ?? 'id');

        // Return results in JSON format
        header('Content-Type: application/json');
        echo json_encode($articles);
    }

    public function deleteOneData($id) {
        $articleRepository = new ArticleRepository();
        $success = $articleRepository->deleteById($id);

        if ($success) {
            http_response_code(200);
            echo json_encode(['message' => 'Article deleted successfully.']);
        } else {
            http_response_code(500);
            echo json_encode(['message' => 'Failed to delete article.']);
        }
    }
}

?>