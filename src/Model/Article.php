<?php

namespace Nassim\Src\Model;

class Article {
    
    private ?int $id = null;
    private string $title;
    private string $content;
    private \DateTime $createdAt;

    public function __construct(?int $id, string $title, \DateTime $createdAt, string $content)
    {
        $this->id = $id;
        $this->title = $title;
        $this->createdAt = $createdAt;
        $this->content = $content;

    }
    
    /* GETTERS and SETTERS */

    // Id

    public function setId($id) {
        $this->id = $id;
    }

    public function getId() {
        return $this->id;
    }

    // Title

    public function setTitle($title) {
        $this->title = $title ;
    }

    public function getTitle() {
        return $this->title;
    }

    // Content

    public function setContent($content) {
        $this->content = $content ;
    }

    public function getContent() {
        return $this->content ;
    }

    // CreatedAt

    public function setCreatedAt($createdAt) {
        $this->createdAt = $createdAt ;
    }

    public function getCreatedAt() {
        return $this->createdAt ;
    }

}

?>
