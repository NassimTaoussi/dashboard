# Dashboard

A dashboard project 

## Technologies
* PHP(POO)
* Jquery
* PHPQuery
* AJAX
* HTML5
* CSS3

## How to launch the project

Rendez-vous sur mon repo a cet url https://gitlab.com/NassimTaoussi/dashboard

Ensuite cliquer sur l'onglet Code

Récupére le lien HTTPS ou SSH selon vos préférences

Puis lancer dans votre terminal la commande :

`git clone https://gitlab.com/NassimTaoussi/dashboard.git`

Une fois le projet télécharger, ouvrer le dossier du projet est verifier bien que vous avez le fichier "dashboard.sql" qui n'est tout autre que la base de données (Si non présent, contactez moi)

Lancer PHPMyAdmin ou tout autre SGBD et importer la base de données "dashboard.sql"

Ouvrer le projet "dashboard" avec votre editeur de code.

Si vous avez Composer d'installer, lancer la commande composer install afin de télécharger l'ensemble des librairies necessaire a ce projet.
(Dans le cas ou vous n'avez pas Composer téléchargez et installez le en suivant les étapes de la documentation officielle https://getcomposer.org/download/).


Dans le .env renseigner l'host de votre base de données ainsi que la table du projet en question et votre identifiant et mot de passe.


## How to launch the data script for the database

Ouvrez le projet dans le terminal et lancer la commande :

`cd commands`

Ensuite exécuter cette commande :

`php RunDataScript.php`

## Auteur

Projet réaliser par Nassim Taoussi https://github.com/NassimTaoussi

 L'image du projet a été réaliser par ce graphiste, à ce [lien](https://www.flaticon.com/free-icons/dashboard)